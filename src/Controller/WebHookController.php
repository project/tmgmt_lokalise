<?php

namespace Drupal\tmgmt_lokalise\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Access\AccessResultAllowed;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Site\Settings as DrupalSettings;
use Drupal\tmgmt_lokalise\Service\Helper;
use Drupal\tmgmt_lokalise\Service\Import;
use Drupal\tmgmt_lokalise\Service\Service;
use Drupal\tmgmt_lokalise\Settings;
use Lokalise\LokaliseApiClient;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class WebHookController
 *
 * @package Drupal\tmgmt_lokalise\Controller
 */
class WebHookController extends ControllerBase {

  private const IP_RANGE = [
    '159.69.72.82',
    '94.130.129.39',
    '195.201.158.210',
    '94.130.129.237',
    '195.201.84.53',
    '18.192.113.205',
    '3.67.82.138',
  ];

  /**
   * @return \Drupal\Core\Access\AccessResultInterface
   */
  public function access(): AccessResultInterface {
    $ip_range = self::IP_RANGE;

    // Provide a mechanism to add additional IP addresses, can be used to proxy
    // the lokalise request to a different url.
    $ip_range = array_merge($ip_range, DrupalSettings::get('lokalise_proxy_ips', []));

    $ip_check = in_array(\Drupal::request()
      ->getClientIp(), $ip_range, FALSE);

    return AccessResultAllowed::allowedIf($ip_check);
  }

  /**
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   * @throws \Drupal\tmgmt\TMGMTException
   * @throws \Lokalise\Exceptions\LokaliseApiException
   * @throws \Lokalise\Exceptions\LokaliseResponseException
   */
  public function process(Request $request): JsonResponse {
    $data = Json::decode($request->getContent());

    // Project ID not provided.
    if (!isset($data['project']['id'])) {
      return new JsonResponse(['Invalid Project ID']);
    }

    // Sometimes the request is requested on an environment that is moved to
    // another url. We provide a mechanism to proxy the request to a different
    // url.
    if (DrupalSettings::get('lokalise_proxy_uri') && !empty($data['event'])) {
      $url_mapping = [
        'project.task.closed' => '/tmgmt_lokalise/project_task_closed',
        'team.order.completed' => '/tmgmt_lokalise/team_order_completed',
      ];

      if (isset($url_mapping[$data['event']])) {
        \Drupal::httpClient()->post(DrupalSettings::get('lokalise_proxy_uri') . $url_mapping[$data['event']], [ 'json' => $data]);
      }
    }

    $queue = \Drupal::queue('tmgmt_lokalise_queue');
    $queue->createItem($data);

    return new JsonResponse(['OK']);
  }

}
