<?php

namespace Drupal\tmgmt_lokalise\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\tmgmt_lokalise\Service\Helper;
use Drupal\tmgmt_lokalise\Service\Import;
use Drupal\tmgmt_lokalise\Service\Service;
use Drupal\tmgmt_lokalise\Settings;
use Lokalise\LokaliseApiClient;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Processes tmgmt lokalise item.
 *
 * @QueueWorker(
 *   id = "tmgmt_lokalise_queue",
 *   title = @Translation("TMGMT lokalise queue"),
 *   cron = {"time" = 60}
 * )
 */
class TmgmtLokaliseQueue extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Lokalise\LokaliseApiClient
   */
  private LokaliseApiClient $client;

  /**
   * @var \Drupal\tmgmt_lokalise\Service\Helper
   */
  private Helper $helper;

  /**
   * @var \Drupal\tmgmt_lokalise\Service\Import
   */
  private Import $import;

  /**
   * WebHookController constructor.
   *
   * @param \Drupal\tmgmt_lokalise\Service\Service $lokalise
   * @param \Drupal\tmgmt_lokalise\Service\Helper $helper
   * @param \Drupal\tmgmt_lokalise\Service\Import $import
   */
    public function __construct(
        array $configuration,
        $plugin_id,
        $plugin_definition,
        Service $lokalise,
        Helper $helper,
        Import $import
    ) {
        parent::__construct($configuration, $plugin_id, $plugin_definition);

        $this->client = $lokalise->getClient();
        $this->helper = $helper;
        $this->import = $import;
    }

  /**
   * {@inheritdoc}
   */
    public static function create(
        ContainerInterface $container,
        array $configuration,
        $plugin_id,
        $plugin_definition
    ): self {
        return new static(
            $configuration,
            $plugin_id,
            $plugin_definition,
            $container->get('tmgmt_lokalise.service'),
            $container->get('tmgmt_lokalise.helper'),
            $container->get('tmgmt_lokalise.import'),
        );
    }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    // Find the job based on project ID.
    $project_id = $data['project']['id'];
    if ($job = $this->helper->findJob($project_id)) {
      // Import translated strings.
      $this->import->execute($job);

      // Delete lokalise project if configured that way.
      if ($job->getTranslator()->getSetting(Settings::DELETE_FINISHED)) {
        $this->client->projects->delete($project_id);
      }
    }
  }

}
