<?php

namespace Drupal\tmgmt_lokalise\Service;

use Drupal\Core\Entity\EntityInterface;
use Drupal\facets\Exception\Exception;
use Drupal\tmgmt\Data;
use Drupal\tmgmt\JobInterface;
use Drupal\tmgmt_lokalise\Settings;
use Lokalise\LokaliseApiClient;

/**
 * Class Export
 *
 * @package Drupal\tmgmt_lokalise\Service
 */
class Export {

  private const HOOK_ORDER_COMPLETED = 'team.order.completed';

  private const HOOK_TASK_CLOSED = 'project.task.closed';

  /**
   * @var \Drupal\tmgmt_lokalise\Service\Service
   */
  private Service $lokalise;

  /**
   * @var \Drupal\Core\Entity\EntityInterface|NULL
   */
  private ?EntityInterface $translator;

  /**
   * @var \Lokalise\LokaliseApiClient
   */
  private LokaliseApiClient $client;

  /**
   * @var \Drupal\tmgmt\Data
   */
  private Data $data;

  /**
   * Export constructor.
   *
   * @param \Drupal\tmgmt_lokalise\Service\Service $lokalise
   * @param \Drupal\tmgmt\Data $data
   */
  public function __construct(Service $lokalise, Data $data) {
    $this->lokalise = $lokalise;
    $this->translator = $lokalise->getTranslator();
    $this->client = $lokalise->getClient();
    $this->data = $data;
  }

  /**
   * @param \Drupal\tmgmt\JobInterface $job
   */
  public function execute(JobInterface $job): void {
    try {
      // Load source and target languages for this job.
      $source_language = $this->translator->mapToRemoteLanguage($job->getSourceLangcode());
      $target_language = $this->translator->mapToRemoteLanguage($job->getTargetLangcode());

      // Create project and set target language.
      $label = $job->get('label')->value ?: $job->label() . ' (' . $job->id() . ')';
      $project_id = $this->lokalise->createProject($label, $source_language, $target_language);
      $job->addMessage('Created a new Project in Lokalise with the id: @id', ['@id' => $project_id], 'debug');

      // Upload source data.
      $this->lokalise->uploadProjectData($project_id, $source_language, $this->buildData($job, $project_id));

      // Set job external reference.
      $job->reference = $project_id;

      if ($this->translator->getSetting(Settings::DOWNLOAD_WHEN_READY)) {
        $keys = [];
        $old_fetched_keys =[];
        $page = 1;
        $limit = 500;

        // Only use the untranslated keys when creating an order this prevents
        // overwriting words in the translation memory.
        do {
          $fetched_keys = array_column($this->client->keys->fetchAll($project_id,
            [
              'filter_untranslated' => 1,
              'limit' => $limit,
              'page' => $page
            ])->getContent()['keys'], 'key_id');

          $diff = array_diff($fetched_keys, $old_fetched_keys);
          if (!empty($diff)) {
            $keys[] = $fetched_keys;
          }

          $old_fetched_keys = $fetched_keys;

          $page++;
        }
        while (!empty($diff));

        // Flatten array
        $keys = array_merge(...$keys);

        // Get provider settings.
        $languages = $this->translator->getSetting(Settings::LANGUAGES);
        $default_provider = $this->translator->getSetting(Settings::PROVIDER);
        $provider = $languages[$job->getTargetLangcode()]['provider'] ?: $default_provider;

        // Create order webhook before the order itself.
        if (!$this->translator->getSetting(Settings::REVIEW_IN_LOKALISE)) {
          $this->lokalise->createWebhook($project_id, 'team_order_completed', [self::HOOK_ORDER_COMPLETED]);
        }

        if (!empty($keys)) {
          // Create the order.
          try {
            $this->lokalise->createOrder($project_id, $label, $source_language, $target_language, $keys, $provider);
          }
          catch (\Exception $e) {
            // It could be possible that this process fails when the amount is
            // 0 euro. In that case an order can't be done and the string should
            // be translated in lokalise itself.
            watchdog_exception('lokalise_translation_provider', $e);
          }
        }

        if ($this->translator->getSetting(Settings::REVIEW_IN_LOKALISE)) {
          $group_id = $languages[$job->getTargetLangcode()]['group'];

          // Add project to group.
          $this->lokalise->addProjectToGroup($project_id, $group_id);

          // Create a webhook to get notified when the task if finished.
          $this->lokalise->createWebhook($project_id, 'project_task_closed', [self::HOOK_TASK_CLOSED]);

          // Create the task, check all keys when reviewing, to have proper
          // context
          $keys = array_column($this->client->keys->fetchAll($project_id)->getContent()['keys'], 'key_id');
          $this->lokalise->createTask($project_id, $source_language, $target_language, $group_id, $keys);
        }
      }
    }
    catch (\Exception $e) {
      // In case something went wrong, cleanup project. This will prevent stale
      // projects within lokalise.
      if ($project_id) {
        $this->client->projects->delete($project_id);
      }

      watchdog_exception('lokalise_translation_provider', $e);
      \Drupal::messenger()->addError($e->getMessage());
    }
  }

  /**
   * @param \Drupal\tmgmt\JobInterface $job
   * @param string $project_id
   *
   * @return array
   * @throws \Drupal\tmgmt\TMGMTException
   */
  private function buildData(JobInterface $job, string $project_id): array {
    $items = $job->getItems();
    $data_flat = array_filter($this->data->flatten($job->getData()), [
      $this->data,
      'filterData',
    ]);

    $translatable_content = [];
    foreach ($data_flat as $key => $value) {
      [$tjiid, $data_item_key] = explode('][', $key, 2);

      // Preserve the original data.
      $items[$tjiid]->addRemoteMapping($data_item_key, $project_id);

      // This is the actual text sent to Lokalise for translation.
      $translatable_content[$tjiid][$data_item_key] = $value['#text'];
    }

    return $translatable_content;
  }

}
